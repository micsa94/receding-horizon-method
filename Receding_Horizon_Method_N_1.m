%%
clc;
clear all;
close all;

%%
samples=5;
T_C_mean = zeros(samples,1);
for test=1:samples
    s = 52;
    rh=2;
    samp=6^rh;
    rl=1;
    q=zeros(s+1,1);
    estimations=1000;
    TC=zeros(estimations,1);
    WC = 5;
    SP = 20;
    RC = 10;
    t_mean = zeros(samp,1);
    ymin = zeros(samp,1);

    %%
    for i = 1:s
        r=rand;
        prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
        x(i) = sum(r >= cumsum([0, prob]));
        x(i)=x(i)-1;
    end

    %%
    for i=1:s
        count1 = 0;
        count = 0;
        v = 1;
        w = 1;
        for j=1:samp
            for k=1:rh
                if count1 == 6
                    count1 = 0;
                    v = v + 1;
                end
                if w == 7
                    w=1;
                end
                if v == 7
                    v = 1;
                end
                if k == 1
                    y(k) = v;
                end
                if k == 2
                    y(k) = w;
                    w = w + 1;
                    count1 = count1 + 1;
                end
            end  
            for est=1:estimations
                stock=q(i);
                tc=0;
                for D=1:rh
                    r=rand;
                    prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
                    d = sum(r >= cumsum([0, prob]));
                    d=d-1;
                    if stock <=rl
                        stock = stock + y(D);
                    end
                    stock=stock-d;
                    if stock<0;
                        tc=tc+SP;
                    end
                    if stock>0;
                        tc=tc+stock*WC;
                    end
                    if stock<0
                        stock=0;
                    end
                end
                TC(est)=tc;
            end
            t_mean(j)=mean(TC);
            ymin(j)=y(1);    
        end
        [M(i),I(i)] = min(t_mean);
        Opt_Y(i) = ymin(I(i));
        if q(i)<=rl
            q(i+1)=q(i)+Opt_Y(i);
        end
        if q(i)>rl
            Opt_Y(i)=0;
            q(i+1)=q(i);
        end
        q(i+1)=q(i+1)-x(i);
        if q(i+1)<0
            q(i+1)=0;
        end
    end

    q=0;
    tc=0;
    for i=1:s-1
        if q<=rl
            q=q+Opt_Y(i);
        end
        q=q-x(i);
        if q<0
            tc=tc+SP;
        end
        if q>0
            tc=tc+q*WC;
        end
        if q<0
            q=0;
        end  
    end
    for i=s
        if q<=r
            q=q+Opt_Y(i);
        end
        q=q-x(i);
        if q<0
            tc=tc+SP;
        end
        if q>0
            tc=tc+q*RC;
        end
    end
    T_C_M(test)=(tc);
end
Mean_Cost=mean(T_C_M)