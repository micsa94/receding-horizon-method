%%
clc;
clear all;
close all;

%%
    s = 52;
    rh=3;
    samp=6^rh;
    rl=1;
    q=zeros(s+1,1);
    estimations=1000;
    TC=zeros(estimations,1);
    WC = 5;
    SP = 20;
    RC = 10;
    t_mean = zeros(samp,1);
    ymin = zeros(samp,1);

    %%
    for i = 1:s
        r=rand;
        prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
        x(i) = sum(r >= cumsum([0, prob]));
        x(i)=x(i)-1;
    end

    %%
    for i=1:s
        count1 = 0;
        count2 = 0;
        count = 0;
        z = 1;
        v = 1;
        w = 1;
        for j=1:samp
            for k=1:rh
                if count1 == 6
                    count1 = 0;
                    v = v + 1;
                end
                if count2 == 1
                    count2 = 0;
                    z = z + 1;
                end
                if w == 7
                    w=1;
                end
                if v == 7
                    v = 1;
                    count2 = count2 + 1;
                end
                if k == 1
                    y(k) = z;
                end
                if k == 2
                    y(k) = v;
                end
                if k == 3
                    y(k) = w;
                    w = w + 1;
                    count1 = count1 + 1;
                end
            end  
            for est=1:estimations
                stock=q(i);
                tc=0;
                for D=1:3
                    r=rand;
                    prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
                    d = sum(r >= cumsum([0, prob]));
                    d=d-1;
                    if stock <=rl
                        stock = stock + y(D);
                    end
                    stock=stock-d;
                    if stock<0;
                        tc=tc+SP;
                    end
                    if stock>0;
                        tc=tc+stock*WC;
                    end
                    if stock<0
                        stock=0;
                    end
                end
                TC(est)=tc;
            end
            t_mean(j)=mean(TC);
            ymin(j)=y(1);    
        end
        [M(i),I(i)] = min(t_mean);
        Opt_Y(i) = ymin(I(i));
        if q(i)<=rl
            q(i+1)=q(i)+Opt_Y(i);
        end
        if q(i)>rl
            Opt_Y(i)=0;
            q(i+1)=q(i);
        end
        q(i+1)=q(i+1)-x(i);
        if q(i+1)<0
            q(i+1)=0;
        end
    end

    ql=0;
    tc=0;
    for i=1:s-1
        if ql<=rl
            ql=ql+Opt_Y(i);
        end
        ql=ql-x(i);
        if ql<0
            tc=tc+SP;
        end
        if ql>0
            tc=tc+ql*WC;
        end
        if ql<0
            ql=0;
        end  
    end
    for i=s
        if ql<=r
            ql=ql+Opt_Y(i);
        end
        ql=ql-x(i);
        if ql<0
            tc=tc+SP;
        end
        if ql>0
            tc=tc+ql*RC;
        end
    end
    tc
